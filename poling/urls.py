from django.urls import path

from . import views as view

app_name = 'poling'
urlpatterns = [
	# /polls
    path('', view.IndexView.as_view(), name='index'),
    # /polls/5
    path('<int:pk>/', view.DetailView.as_view(), name='detail'),
    # /polls/5/results
    path('<int:pk>/results/', view.ResultsView.as_view(), name='results'),
    # /polls/5/vote
    path('<int:question_id>/vote/', view.vote, name='vote'),

]
