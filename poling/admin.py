from django.contrib import admin
from .models import Question,Choice
# Register your models here.

class ChoiceInLine(admin.TabularInline):
	model = Choice
	extra = 3

class QuestionAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,{
			'fields': ['question_text'],}),
		('Date Information', {
			'fields': ['pub_date'],
			'classes': ['collapse'],})
	]
	inlines = [ChoiceInLine]
	
	list_display = (
			'question_text',
			'pub_date',
			'was_published_recently',
	)

	#Add Filter
	list_filter = ['pub_date']

	#Add Search
	search_fields = ['question_text']

class ChoiceAdmin(admin.ModelAdmin):
	list_display = (
			'choice_text',
			'votes',
		)

	#Add filter
	list_filter = ['votes', 'question']		

admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)